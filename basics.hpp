#ifndef BASICS
#define BASICS

#include <vector>
#include <memory>
#include <algorithm>


//indexing

//index sequence 
std::vector<int> idxseq(int n1){
  std::vector<int> ans;
  for (auto ii=0;ii<n1;++ii){
    ans.push_back(ii);
  }
  return ans;
}

//endpoint exclusive 
std::vector<int> idxseq(int start, int stop1 ){
  std::vector<int> ans;
  for (auto ii=start;ii<stop1;++ii){
    ans.push_back(ii);
  }
  return ans;
}

//endpoint exclusive 
std::vector<int> idxseq(int start, int stop1, int stride){
  std::vector<int> ans;
  for (auto ii=start;ii<stop1; ii+=stride){
    ans.push_back(ii);
  }
  return ans;
}

struct allseq{};

//convert a single index to a multi-index
//c-style ordering: first index varies slowest
//zero - indexed
// b = "bases"
// n = ans[bl-1]
//   + ans[bl-2]*b[bl-1]
//   + ans[bl-3]*b[bl-1]*b[bl-2]
//   +  ...
std::vector<int> multi_idx(std::vector<int> b, int n){
  int bl = b.size();
  std::vector<int> ans(bl, 0);
  int temp = n;
  for( int ii=0; ii<bl; ++ii){
    auto rq = std::div(temp,b[bl-1-ii]);
    temp = rq.quot;
    ans[bl-1-ii]= rq.rem;
  }
  return ans;
}

//convert a multi-index to a single index
//c-style ordering: first index varies slowest
//zero - indexed
// inverse of multi_idx
int single_idx(std::vector<int> b, std::vector<int> mi){
  int bl = mi.size();
  int ans = 0;
  for (int ii=0; ii<bl; ++ii){
    int temp = mi[bl-1-ii];
    for (int jj=0; jj<ii; ++jj){
      temp = temp * b[bl-1-jj];
    }
    ans = ans + temp; 
  }
  return ans;
}

//sorting

template<class T>
std::vector<int> sortperm(const std::vector<T> & v){
  std::vector<int> idxs;
  for (int i=0; i<v.size(); i++){
    idxs.push_back(i);
  }

  auto comp = [&](const int & a, const int & b){
    return v[a] < v[b];
  };
  sort(idxs.begin(), idxs.end(), comp);

  return idxs;
}

#endif
